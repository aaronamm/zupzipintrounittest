﻿using System;
using System.IO;
using System.Reflection;
using System.Security.Policy;
using AppDomainToolkit;
using ZupZip.Lib.Interfaces;

namespace ZupZip.CourseTool
{
    public class Program
    {
        private const string ASSEMBLY_PATH =
                        @"C:\Projects\ZupZipIntroUnitTest\ZupZip.Lib\bin\Debug\ZupZip.Lib.dll";

        private const string ASSEMBLY_URL =
                        @"http://codesanook.cloudapp.net/dll/ZupZip.Lib.dll";
        public static void Main(string[] args)
        {
           LoadRemotely();
            //LoadLocally();
        }

        public static void LoadLocally()
        {
            var appDomain = AppDomain.CreateDomain("dynamicDll");
            appDomain.DomainUnload += new EventHandler(appDomain_DomainUnload);
            var proxy = (Proxy)appDomain.CreateInstanceAndUnwrap(
                typeof(Proxy).Assembly.FullName,
                typeof(Proxy).FullName);

            proxy.LoadAssembly(ASSEMBLY_PATH);

            AppDomain.Unload(appDomain);

            File.Delete(ASSEMBLY_PATH);//you can delete referece dll after remove 

        }

        public static void LoadRemotely()
        {
            var appDomain = AppDomain.CreateDomain("dynamicDll");
            appDomain.DomainUnload += new EventHandler(appDomain_DomainUnload);
            appDomain.AssemblyResolve += new ResolveEventHandler(appDomain_AssemblyResolve);  
            var proxy = (Proxy)appDomain.CreateInstanceAndUnwrap(
                typeof(Proxy).Assembly.FullName,
                typeof(Proxy).FullName);

            proxy.LoadAssembly(ASSEMBLY_URL);

            AppDomain.Unload(appDomain);
        }

        static Assembly appDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        {
            Console.WriteLine("assembly name: {0}", args.Name);
            Console.ReadKey();
            return null;
        }

        public static void appDomain_DomainUnload(object sender, EventArgs e)
        {
            Console.WriteLine("domain loaded sender: {0}",
                sender.GetType().FullName);
        }
    }



    public class Proxy : MarshalByRefObject
    {
        public void LoadAssembly(string assemblyPath)
        {
            try
            {
                //this dll will load in new domain
                var assembly = Assembly.LoadFrom(assemblyPath);

                var type = assembly.GetType("ZupZip.Lib.GradeCalculator");
                var gradeCalculator = (IGradeCalculator)Activator.CreateInstance(type);
                var score = 80;
                Console.WriteLine("you got grad: {0} from score: {1}",
                    gradeCalculator.GetGradeForScore(score),
                    score);

            }
            catch (Exception ex)
            {
                // throw new InvalidOperationException(ex);
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
            }
        }
    }
}