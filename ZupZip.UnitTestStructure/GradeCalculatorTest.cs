﻿using System;
using NUnit.Framework;
using ZupZip.Lib;

namespace ZupZip.UnitTestStructure
{
    public class GradeCalculatorTest
    {

        private GradeCalculator gradeCalculator;

        [TestFixtureSetUp]
        public void BeforeEachTest()
        {
             gradeCalculator = new GradeCalculator();
        }

        [Test]
        public void GetGradeForScore_100_ReturnGradeA()
        {
            //arrange
            var score = 100;
            var expectedGrade = "A";

            //act
            var actualGrade = gradeCalculator.GetGradeForScore(score);

            //assert
            Assert.AreEqual(expectedGrade,actualGrade);
        }

        [Test]
        public void GetGradeForScore_85_ReturnGradeA()
        {
            //arrange
            var score = 85;
            var expectedGrade = "A";

            //act
            var actualGrade = gradeCalculator.GetGradeForScore(score);

            //assert
            Assert.AreEqual(expectedGrade,actualGrade);
        }

        [Test]
        public void GetGradeForScore_75_ReturnGradeA()
        {

            //arrange
            var score = 75;
            var expectedGrade = "B";

            //act
            var actualGrade = gradeCalculator.GetGradeForScore(score);

            //assert
            Assert.AreEqual(expectedGrade,actualGrade);
        }


        [Test]
        public void GetGradeForScore_69_ReturnGradeC()
        {

            //arrange
            var score = 69;
            var expectedGrade = "C";

            //act
            var actualGrade = gradeCalculator.GetGradeForScore(score);

            //assert
            Assert.AreEqual(expectedGrade, actualGrade);
        }

        [Test]
        public void GetGradeForScore_50_ReturnGradeD()
        {

            //arrange
            var score = 50;
            var expectedGrade = "D";

            //act
            var actualGrade = gradeCalculator.GetGradeForScore(score);

            //assert
            Assert.AreEqual(expectedGrade,actualGrade);
        }


        [Test]
        public void GetGradeForScore_49_ReturnGradeF()
        {

            //arrange
            var score = 49; 
            var expectedGrade = "F";

            //act
            var actualGrade = gradeCalculator.GetGradeForScore(score);

            //assert
            Assert.AreEqual(expectedGrade,actualGrade);
        }

        [Test]
        public void GetGradeForScore_0_ReturnGradeF()
        {

            //arrange
            var score = 0;
            var expectedGrade = "F";

            //act
            var actualGrade = gradeCalculator.GetGradeForScore(score);

            //assert
            Assert.AreEqual(expectedGrade,actualGrade);
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void GetGradeForScore_ScoreLessThan0_ReturnGradeF()
        {
            //arrange
            var invalidScore = -1;

            //act
            gradeCalculator.GetGradeForScore(invalidScore);
        }


        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void GetGradeForScore_ScoreGreather100_ReturnGradeF()
        {
            //arrange
            var invalidScore = 101;

            //act
            gradeCalculator.GetGradeForScore(invalidScore);

        }


        [Test]
        public void GetGradeForScore_CallSameObjectScore80_ReturnGradeB()
        {
            var grade = gradeCalculator.GetGradeForScore(0);
            Assert.AreEqual("F", grade);

             grade = gradeCalculator.GetGradeForScore(80);
            Assert.AreEqual("B", grade);
        }


    }
}
