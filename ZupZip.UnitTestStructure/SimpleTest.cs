﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace ZupZip.UnitTestStructure
{
    [TestFixture]
    public class SimpleTest
    {
        private double PlusNumber(double num1, double num2)
        {
            return num1 + num2;
        }

        /// <summary>
        /// success test
        /// </summary>
        [Test]
        public void PlusNumber_Input45And55_Return100()
        {
            //arrange 1st A
            var num1 = 45;
            var num2 = 55;

            //act 2nd A
            var actualResult = PlusNumber(num1, num2);

            //assert 3rd A
            const int expectedResult = 100;
            Assert.AreEqual(expectedResult, actualResult);
        }

        /// <summary>
        /// success test
        /// </summary>
        [Test]
        public void PlusNumber_Input21And79_Return100()
        {
            //arrange 1st A
            var num1 = 21;
            var num2 = 79;

            //act 2nd A
            var actualResult = PlusNumber(num1, num2);

            //assert 3rd A
            const int expectedResult = 99;
            Assert.AreEqual(expectedResult, actualResult);
        }
    }
}
