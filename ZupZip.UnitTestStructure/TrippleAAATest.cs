﻿using System;
using NUnit.Framework;

namespace ZupZip.UnitTestStructure
{

    [TestFixture]
    public class TrippleAaaTest
    {

        [TestFixtureSetUp]
        public void RunBeforeFirstTest()
        {
            Console.WriteLine("TestFixtureSetUp called");
        }


        [TestFixtureTearDown]
        public void RunAfterAllTestsDone()
        {
            Console.WriteLine("TestFixtureTearDown called");
        }


        [SetUp]
        public void RunBeforEachTest()
        {
            Console.WriteLine("SetUp called");
        }


        [TearDown]
        public void RunAfterEachTest()
        {
            Console.WriteLine("TearDown called");
        }


        [Test]
        public void TestMethod()
        {
        
            Console.WriteLine("Test Method called");
        }

        [Test]
        public void TestMethod2()
        {
        
            Console.WriteLine("Test Method called");
        }

    }
}
