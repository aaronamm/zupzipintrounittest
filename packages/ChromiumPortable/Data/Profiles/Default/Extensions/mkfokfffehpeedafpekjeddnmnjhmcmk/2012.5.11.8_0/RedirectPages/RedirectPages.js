function GetEmbedObejct() {
    var embedObject = document.getElementById('cosymantecbfw');
    if (null == embedObject) {
        var embedElement = document.createElement('embed');
        embedElement.setAttribute('type', '{0C55C096-0F1D-4F28-AAA2-85EF591126E7}');
        embedElement.setAttribute('cotype', 'cs');
        embedElement.setAttribute('id', 'cosymantecbfw');
        embedElement.setAttribute('style', 'width: 0px; height: 0px; display: block;');
        embedObject = document.documentElement.appendChild(embedElement)
    }
    return embedObject;
}
        
var embedObject;
function init(){
    embedObject = GetEmbedObejct();  
    chrome.extension.sendRequest({ contentscript: "GetTabInfo" }, function(response) {
            embedObject.SetId(response.WindowId, response.TabId, response.IncognitoMode, response.WindowType);
	    embedObject.Nav(window == window.top, true);
	    embedObject.DOMContentLoaded(window == window.top)
	    embedObject.Nav(window == window.top, false);
	    checkBadUrl();

        });
}   

function checkBadUrl()
{
    var paraData = document.getElementById('BadURL');
    if('&nbsp;' == paraData.innerHTML)
    {
        window.open('about:blank', '_self');
    }
}            
