﻿using System;
using System.Collections.Generic;
using System.Linq;
using Moq;
using NUnit.Framework;
using ZupZip.Commerce.Actionresults;
using ZupZip.Commerce.Controllers;
using ZupZip.Commerce.Interfaces;
using ZupZip.Commerce.Models;
using ZupZip.Commerce.ViewModels;

namespace ZupZip.Commerce.Tests
{
    [TestFixture]
    public class ProductControllerTest
    {
        private IProductRepository productRepository;
        private ProductController productController;

        [SetUp]
        public void BeforeEachTest()
        {

            // create some mock products to play with
            IList<Product> products = new List<Product>
                {
                    new Product
                        {
                            Id = 1,
                            Name = "C# Unleashed",
                            Description = "Short description here",
                            Price = 40
                        },
                    new Product
                        {
                            Id = 2,
                            Name = "ASP.Net Unleashed",
                            Description = "Short description here",
                            Price = 50
                        },
                    new Product
                        {
                            Id = 3,
                            Name = "Silverlight Unleashed",
                            Description = "Short description here",
                            Price = 30
                        }
                };

            // Mock the Products Repository using Moq
            var mockProductRepository = new Mock<IProductRepository>();

            // Return all the products
            mockProductRepository.Setup(mr => mr.FindAll()).Returns(products);


            mockProductRepository.Setup(mr =>
                                        mr.FindByIdList(It.IsAny<int[]>())
                ).Returns((int[] productIds) => products.Where(p => productIds.Contains(p.Id)).ToList());


            // return a product by Id
            mockProductRepository.Setup(mr => mr.FindById(
                It.IsAny<int>())).Returns((int i) => products.Single(x => x.Id == i));

            // return a product by Name
            mockProductRepository.Setup(mr => mr.FindByName(
                It.IsAny<string>())).Returns((string s) => products.Single(x => x.Name == s));

            // Allows us to test saving a product
            mockProductRepository.Setup(mr => mr.Save(It.IsAny<Product>())).Returns(
                (Product newProduct) =>
                {
                    var now = DateTime.Now;
                    newProduct.DateCreated = now;
                    newProduct.DateModified = now;
                    newProduct.Id = products.Count() + 1;
                    products.Add(newProduct);
                    return newProduct;
                });

            // Complete the setup of our Mock Product Repository
            productRepository = mockProductRepository.Object;

            productController = new ProductController(productRepository);
        }

        [Test]
        public void Create_ValidInput_ReturnCreateProductWithId4()
        {
            // Create a new product, not I do not supply an id
            var newProduct = new Product { Name = "Pro C#", Description = "Short description here", Price = 39.99 };
            var actionResult = (JsonResponseResult<RequestResult>)productController.Create(newProduct);
            var requestResult = (RequestResult<Product>)actionResult.ResultData;

            Assert.AreEqual(4, requestResult.Data.Id);
        }

        [Test]
        public void Create_ProductNameIsNull_ReturnIsSuccessFalse()
        {
            // Create a new product, not I do not supply an id
            var newProduct = new Product { Name = null, Description = "Short description here", Price = 39.99 };
            var actionResult = (JsonResponseResult<RequestResult>)productController.Create(newProduct);
            var requestResult = actionResult.ResultData;

            Assert.IsFalse(requestResult.IsSuccess);
        }


        [Test]
        public void CalculateTotalPrice_ProductId1And2And3_Return120Dollars()
        {
            // Create a new product, not I do not supply an id
            var productIds = new[] { 1, 2 ,3 };
            var actionResult = (JsonResponseResult<RequestResult>)productController.CalculateTotalPrice(productIds);
            var requestResult = (RequestResult<double>)actionResult.ResultData;

            var totalPrice = 120;
            Assert.AreEqual(totalPrice, requestResult.Data);
        }

        [Test]
        public void Detail_ProductId3_ReturnProductId3()
        {
            // Try finding a product by id
            Product testProduct = this.productRepository.FindById(2);

            Assert.IsNotNull(testProduct); // Test if null
            Assert.IsInstanceOf(typeof(Product), testProduct); // Test type
            Assert.AreEqual("ASP.Net Unleashed", testProduct.Name); // Verify it is the right product
        }

        [Test]
        public void Details_ValidProductName_ReturnProductWithGivenName()
        {
            // Try finding a product by Name
            Product testProduct = this.productRepository.FindByName("Silverlight Unleashed");

            Assert.IsNotNull(testProduct); // Test if null
            Assert.IsInstanceOf(typeof(Product), testProduct); // Test type
            Assert.AreEqual(3, testProduct.Id); // Verify it is the right product
        }

        [Test]
        public void Index_ReturnAllProducts()
        {
            // Try finding all products
            var testProducts = this.productRepository.FindAll();

            Assert.IsNotNull(testProducts); // Test if null
            Assert.AreEqual(3, testProducts.Count); // Verify the correct Number
        }

        [Test]
        public void Edit_ChangeProductName_ProductNameChange()
        {
        }



    }

}
