﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZupZip.Lib.Interfaces
{
    public interface IGradeCalculator
    {
        string GetGradeForScore(double score);
    }
}
