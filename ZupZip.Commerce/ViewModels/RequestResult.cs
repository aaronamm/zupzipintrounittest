﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZupZip.Commerce.ViewModels
{

    public class RequestResult
    {
        public RequestResult()
        {
            IsSuccess = true;
        }

        public RequestResult(Exception ex)
        {
            IsSuccess = false;
            ErrorMessage = ex.Message;
        }

        public bool IsSuccess { get; set; }
        public string ErrorMessage { get; set; }
    }


    public class RequestResult<T>:RequestResult
    {

        public T Data { get; set; }

        public RequestResult(T data)
        {
            this.Data = data;
        }


    }
}