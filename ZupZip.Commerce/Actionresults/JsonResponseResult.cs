﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace ZupZip.Commerce.Actionresults
{
    public class JsonResponseResult<T> : ActionResult
    {
        public T ResultData { get; set; }

        public JsonResponseResult(T resultData)
        {
            this.ResultData = resultData;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            if (context != null && context.HttpContext != null)
            {
                var jsonString = JsonConvert.SerializeObject(ResultData);

                var contextBase = context.HttpContext;
                contextBase.Response.Buffer = true;
                contextBase.Response.Clear();
                contextBase.Response.ContentType = "application/json";
                contextBase.Response.Write(jsonString);

            }


        }
    }
}