﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZupZip.Commerce.Models;

namespace ZupZip.Commerce.Interfaces
{
    public interface IProductRepository
    {
        IList<Product> FindAll();

        Product FindByName(string productName);

        Product FindById(int productId);

        IList<Product> FindByIdList(int[] productIds);

        Product Save(Product target);

        bool Delete(int productId);

        void SaveChange();
    }
}