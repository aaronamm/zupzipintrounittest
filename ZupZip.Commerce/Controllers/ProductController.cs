﻿using System;
using System.Linq;
using System.Web.Mvc;
using ZupZip.Commerce.Actionresults;
using ZupZip.Commerce.Interfaces;
using ZupZip.Commerce.Models;
using ZupZip.Commerce.ViewModels;

namespace ZupZip.Commerce.Controllers
{
    public class ProductController : Controller
    {
        public IProductRepository productRepository;

        public ProductController()
        {
        }


        public ProductController(IProductRepository productRepository)
        {
            this.productRepository = productRepository;
        }

        public ActionResult Index()
        {
            var products = productRepository.FindAll();
            return View(products);
        }


        [HttpPost]
        public ActionResult Create(Product product)
        {
            RequestResult result = null;
            try
            {
                if (string.IsNullOrEmpty(product.Name))
                {
                    throw new InvalidOperationException("product Name cannot be null");
                }
                var savedProduct = productRepository.Save(product);
                result = new RequestResult<Product>(savedProduct);
            }
            catch (Exception ex)
            {
                result = new RequestResult(ex);
            }

            return new JsonResponseResult<RequestResult>(result);
        }


        public ActionResult Details(int id)
        {
            var product = productRepository.FindById(id);
            return View(product);
        }

        public ActionResult Edit(int id, Product updatedProduct)
        {
            var productFromDb = productRepository.FindById(id);
            //update
            productFromDb.Name = updatedProduct.Name;
            productFromDb.Price = updatedProduct.Price;
            productFromDb.Description = updatedProduct.Description;
            productFromDb.DateModified = DateTime.Now;

            productRepository.SaveChange();
            return View();
        }


        public ActionResult Delete(int id)
        {
            var result = productRepository.Delete(id);
            return View(result);
        }


        [HttpPost]
        public ActionResult CalculateTotalPrice(int[] productIds)
        {
            RequestResult requestResult;
            try
            {
                var products = productRepository.FindByIdList(productIds);
                var totalPrice = products.Sum(p => p.Price);
                requestResult = new RequestResult<double>(totalPrice);
            }
            catch (Exception ex)
            {
                requestResult = new RequestResult(ex);
            }
            return new JsonResponseResult<RequestResult>(requestResult);
        }

    }
}
